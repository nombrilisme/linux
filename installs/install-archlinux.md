---
title: "install archlinux"
type: term
domain: linux
back: "../../"
---


asus q325ua

# prelims

- flash the archlinux iso to a live usb
- boot the installer (hit power and hold down F2)
- once in the environment connect to wifi: 


wifi-menu			
ip addr show	
ping archlinux.org	


- update package repository indexes:


pacman -Syyy


- set date and time:


timedatectl set-ntp true


# disk partitioning 

- many ways to do this
- tried initially with LVM encryption but didn't work (my incompetence)
- ended up creating 3 partitions: EFI, root, and home
- to see current partition structure:


fdisk -l	


- to start partitioning on /dev/sda:


fdisk /dev/sda


- some commands commands once in the fdisk utility:
	+ `p`: print current partition structure
	+ `g`: create empty GPT table
	+ `n`: create new partition
	+ `t`: change partition type
	+ `w`: finalise partitions

- make `/dev/sda1` the EFI partition, size +512M, type "EFI filesystem"
- make `/dev/sda2` the root partition, size +32G, type "Linux filesystem"
- make `/dev/sda3` the home partition, size [rest of drive], type "Linux 
  filesystem"
- format the partitions:


mkfs.fat -F32 /dev/sda1
mkfs.ext4 /dev/sda2
mkfs.ext4 /dev/sda3


- mount root partition to /mnt:


mount /dev/sda2 /mnt


- mount other partitions within /mnt:


mkdir /mnt/home
mount /dev/sda3 /mnt/home
mkdir /mnt/efi
mount /dev/sda1 /mnt/efi


# install archlinux base packages 

- install base packages:


pacstrap -i /mnt base linux linux-firmware mkinitcpio vi 


- generate fstab file:


genfstab -U -p /mnt >> /mnt/etc/fstab
cat /mnt/etc/fstab 			#verify contents of fstab file


# basic arch setup in the new environment 

- access in-progress arch installation:


arch-chroot /mnt


- install lots of packages:


pacman -S  \ 
linux-headers base-devel \ 
grub efibootmgr dosfstools openssh os-prober mtools \ 
nano vim \ 
networkmanager network-manager-applet wpa_supplicant wireless_tools \ 
dialog


- enable NetworkManager (very important):


systemctl enable NetworkManager


- create initial ramdisk for kernel:


mkinitcpio -p linux


- generate locale: run `vim /etc/locale.gen` and uncomment line with `en_US.UTF-8`
- then generate the locale by running:


locale-gen


- set root password:


passwd


- create user account:


useradd -m -g users -G wheel <uname>
passwd <uname>	


- install sudo (if not yet for some reason):


pacman -S sudo


- allow users in wheel group to use sudo:


visudo 


- (and uncomment the line `%wheel ALL=(ALL) ALL`)
- set up grub and efi:


grub-install --target=x86_64-efi --efi-directory=/efi --bootloader-id=grub_uefi --recheck 


- generate grub config file:


grub-mkconfig -o /boot/grub/grub.cfg


- create a swapfile (supposedly this is better than having an entire swap 
  partition):


fallocate -l 4G /swapfile
chmod 600 /swapfile
mkswap /swapfile
echo '/swapfile none swap sw 0 0' | tee -a /etc/fstab


# exit the installation, unmount, reboot 


exit
umount -a
reboot		# make sure to quickly remove the usb


# post setup: WIFI (the moment of truth) 

- (re)boot into the new OS (sans usb)
- connect to wifi:


nmcli d wifi list
nmcli d wifi connect <BSSID> password <password>


- if this doesn't work, well, it's all over
- may as well start again (?) or troubleshoot
- this step failed for me many times over three days of installing/reinstalling
- I think the trick is to ensure there is only one network manager trying to 
  configure wifi. in my preliminary installs I had both NetworkManager and 
  netctl (because I did various things from various different wikis/blogs) 
  which didn't work 
- useful commands to check what's going on with wifi: 
	+ `lspci -k`: check if driver has been installed and works
	+ `ip link`: check names/connectivity status of devices
	+ `ip a(ddr show)`: show devices and IP addresses
	+ `systemctl enable NetworkManager`: enable NetworkManager to manage 
	+ `nmcli d`: show device list 
	+ `wifi-menu`: works with the dialog package to connect 

# set up pacman and yay 

- edit pacman mirrorlist:


vim /etc/pacman.d/mirrorlist 	#put closest one(s) at the top


- apparently there's an arch thing that will generate the best mirrorlist
- set up AUR: `vim /etc/pacman.conf`
- then put this at end of the `pacman.conf` file:


[archlinuxfr]
Server = http://repo.archlinux.fr/$arch


- install yay:


sudo pacman -S git	
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si 


- to install packages from yay:


yay -S <packagename>


- to remove packages from yay:


yay -Rns <packagename>


# set up X window system 

- arch packages:
	+ `xf86-input-libinput`
	+ `xorg-server` 
	+ `xorg-xinit` 
	+ `xorg-apps` 
	+ `mesa`

- yay packages:
	+ `xf86-video-intel` 
	+ `lib32-intel-dri` 
	+ `lib32-mesa` 
	+ `lib32-libgl` 

- also:


sudo pacman -S xorg-twm xorg-sclock xterm xorg-xrdb 


- run:


startx	


- this should start the X window system


# set up i3 wm 

- packages:


sudo pacman -S i3-gaps i3blocks i3lock i3status 


- more packages for i3: ranger
- create copy of default xinitrc in home directory:


cp /etc/X11/xinit/xinitrc ~/.xinitrc


- put `exec i3` at the end of the .xinitrc file
- see my example .xinitrc file
- run `startx` to start an i3 session

# Xresources 

- .Xresources file: used to set terminal colours, fonts, cursor style
- make an .Xresources file:


touch ~/.Xresources


- to enact a change in the .Xresources file:


xrdb ~/.Xresources


- link colors in the i3 config to those in .Xresources
