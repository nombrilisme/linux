# lineage install 

sony xperia xa2 h3123

## prelims

- download android platform tools to computer
- read [this link][1]
- put the platform-tools directory somewhere convenient 
- when people refer to an "adb terminal", they mean a terminal in the 
  `platform-tools` directory

## unlock bootloader of phone

- yeah so just do that

## root phone

- ditto
- read [this][2] (an ok guide)

## install twrp recovery

- install [twrp recovery][3] (there's a different img and zip file for each phone)

## boot into twrp recovery mode

- do one of these:
	+ 1) figure out how to do it for the specific phone. usually hold down power 
	  and vol up/down buttons. if twrp is installed it should boot right into twrp 
	  recovery. 
	+ 2) boot into fastboot mode and connect to computer:
		+ on xperia xa2, hold down vol up for two seconds and then plug into 
		  computer
		+ then type `./fastboot boot twrp.img` into an adb terminal

## download and install lineage

- download the [lineage zip file][4] and save to platform-tools folder [on computer]
	+ (optional) rename file to lineage.zip
- in the twrp recovery menu, click Advanced -> ADB Sideload then swipe to 
  start sideload
- type `./adb sideload lineage.zip` and wait for process to finish
- go back to the main menu and click Reboot -> System
- should boot into the new lineage environment

## installing apps 

- open a browser and search for the .apk file
- download the .apk file
- go to Files and try clicking on the .apk file. if it doesn't open, move the 
  file to the root directory, and open it there
	+ this worked for the xperia xa2

## some observations about lineage

- firefox is slow
- niagara launcher works 
- many apps punish for not participating in google 
- the nyc ferry app doesn't work
- citymapper doesn't really work

## overall lineage experience

- bad

[1]: https://www.droidviews.com/download-latest-adb-and-fastboot-sdk-platform-tools-win-mac-linux/
[2]: https://www.droidviews.com/xperia-xa2-root-twrp/
[3]: https://twrp.me/Devices/
[4]: https://androidfilehost.com/?w=files&flid=281514
